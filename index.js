// 1. convert raw data into array, then object
// 2. Add unix time and severity to object property
// 3. divide data into batt and tstat, remove data that don't exceed red zones
// 4. Sort data based on ID first, then unix time
// 5. find time differences that exceed 5 minutes

const fileInput = document.querySelector('input');

fileInput.addEventListener('change', () => {
  const fr = new FileReader();
  fr.readAsText(fileInput.files[0])
  fr.addEventListener('load', () => {

    const thatHave = {
      batt: 'BATT',
      tstat: 'TSTAT',
      redLow: 'RED LOW',
      redHigh: 'RED HIGH',
      lessThanLimit: e => e.rawValue < e.redLowLimit,
      greaterThanLimit: e => e.rawValue > e.redHighLimit,
    }

    function rawDataToObject(measurementType) {
      return fr.result
        .split('\r\n')
        .filter(Boolean)
        .map(e => { return e.split('|') })
        .filter(e => { return e.includes(measurementType) })
        .map(e => {
          const timeFormat = e[0]
            .slice(0, 4)
            .concat('-')
            .concat(e[0].slice(4, 6))
            .concat('-')
            .concat(e[0].slice(6, 8))
            .concat('T')
            .concat(e[0].slice(9))
            .concat('Z')
          return {
            timestamp: timeFormat,
            timeUnix: Date.parse(timeFormat),
            satelliteId: Number(e[1]),
            redHighLimit: Number(e[2]),
            yellowHighLimit: Number(e[3]),
            yellowLowLimit: Number(e[4]),
            redLowLimit: Number(e[5]),
            rawValue: Number(e[6]),
            component: e[7],
          }
        })
    }

    function exceedsLimits(measurementType, limitColor, limitFunction) {
      return rawDataToObject(measurementType)
        .filter(limitFunction)
        .map(e => {
          return { ...e, severity: limitColor }
        })
    }

    const battObj = exceedsLimits(thatHave.batt, thatHave.redLow, thatHave.lessThanLimit)
    const tstatObj = exceedsLimits(thatHave.tstat, thatHave.redHigh, thatHave.greaterThanLimit)

    function fiveMinBreach(argObj, finalResults) {
      argObj = argObj.sort((x, y) => x.satelliteId - y.satelliteId || x.timeUnix - y.timeUnix)

      for (let i = 0, j = i + 1, count = 0; j < argObj.length; j++) {
        if (argObj[j].timeUnix - argObj[i].timeUnix < 300000 && argObj[j].satelliteId === argObj[i].satelliteId) {
          count++;
          if (count === 2) {
            finalResults.push(
              {
                satelliteId: argObj[i].satelliteId,
                severity: argObj[i].severity,
                component: argObj[i].component,
                timestamp: argObj[i].timestamp,
              }
            )
          }
        }
        else {
          if (count >= 2) {
            i = j;
            count = 0
          }
          else {
            i++
            count = 0
          }
        }
      }
    }

    function executeMain() {
      let finalResults = [];
      fiveMinBreach(tstatObj, finalResults)
      fiveMinBreach(battObj, finalResults)
      console.log(JSON.stringify(finalResults))
      return document.getElementById('preview').innerHTML=JSON.stringify(finalResults)
    }

    executeMain()

  })
})